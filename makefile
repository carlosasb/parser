include make.config
#----------------------------------------------------------------------#
.SUFFIXES: .cpp .o .h 

mainsrc = $(MAIN).cpp
mainobj = $(MAIN).o

sources = parser.cpp $(mainsrc)

.cpp.o:
	$(CPPC) $(FFLAGS) -c $*.cpp

objects = parser.o

objects += $(mainobj)

$(NAME): $(objects)
	$(CPPC) $(FFLAGS)  $(objects) -o $(NAME).x

clean:
	rm -rf *.o *.x *.mod *~ .*~ $(NAME).x
run:
	./$(NAME).x

#-----------------------------------------------------------------------#
parser.o:    makefile parser.cpp parser.h
$(mainobj):  makefile $(mainsrc) parser.o parser.h
