#include <iostream>

using namespace std;

#include "parser.h"

int main()
{
    string teststr = "This is a test", teststr2 = "This_is_a_test";
    char new_delim = '_';
    parser token1(' ');

    /*------------------------------------------------------------------------------*/

    token1.re_init(teststr);

    cout << token1.ntoken() << '\n';

    for (int i = 0; i < token1.ntoken(); i++) {cout << token1.select_token(i) << '\n';}

    /*------------------------------------------------------------------------------*/

    cout << '\n';

    token1.re_init(new_delim);

    cout << token1.ntoken() << '\n';

    for (int i = 0; i < token1.ntoken(); i++) {cout << token1.select_token(i) << '\n';}

    /*------------------------------------------------------------------------------*/

    cout << '\n';

    token1.re_init(teststr2,new_delim);

    cout << token1.ntoken() << '\n';

    for (int i = 0; i < token1.ntoken(); i++) {cout << token1.select_token(i) << '\n';}

    return 0;
}
