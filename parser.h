#include <vector>
#include <string>
#include <sstream>

class parser
{
    private:
        std::vector<std::string> elems;
        char delim;
        std::string str;
    public:
        parser();
        parser(const std::string&);
        parser(const std::string&, char);
        parser(const char);
        /* */
        int ntoken();
        std::string select_token(int);
        std::vector<std::string> tokenize();
        /* */
        void reset() {elems.clear();};
        void re_init(const std::string&);
        void re_init(const char);
        void re_init(const std::string&, char);
        /* */
        ~parser();
};
