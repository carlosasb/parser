#include <iostream>
#include "parser.h"

using namespace std;

/* constructors */
parser::parser()
{
    delim = ' ';
    str   = "";
}

parser::parser(const string &s)
{
    delim = ' ';
    str   = s;

    elems = tokenize();
}

parser::parser(const string &s, const char d)
{
    delim = d;
    str   = s;

    elems = tokenize();
}

parser::parser(const char d)
{
    delim = d;
    str   = "";
}

/* reseting the object for different strings and delimiters */
void parser::re_init(const string &s)
{
    str   = "";
    str   = s;

    elems.clear();
    elems = tokenize();
}

void parser::re_init(const char d)
{
    delim = d;
}

void parser::re_init(const string &s, const char d)
{
    delim = d;
    str   = "";
    str   = s;

    elems.clear();
    elems = tokenize();
}

/* method for returning the number of tokens in str */
int parser::ntoken()
{
    int number = 0;
    number = elems.size();
    return number;
}

/* method for selecting specific tokens */
string parser::select_token(int l)
{
    string token = "";
    if (l <= elems.size())
    {
        token = elems[l];
    }
    return token;
}

/* private initializing method */
vector<string> parser::tokenize()
{
    stringstream ss(str);
    string item;
    while (getline(ss,item,delim))
    {
        if (!item.empty())
        {
            elems.push_back(item);
        }
    }
    return elems;
}

/* destructor */
parser::~parser()
{
    elems.clear();
}
